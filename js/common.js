var apiHostUrl = "http://prod-code.com:1100/";
var projectName = "Prod-Code";


//登录接口
var loginUrl = "sys/user/login";
//用户信息
var userInfoUrl = "sys/user/userinfo";

//导航菜单
var navUrl = "sys/nav";

//localStorage的值
var tokenStr = "token"; 


//session 的key
var userInfo = "userInfo";

var bserUrl = "http://prod-code.com/";

//需要登录的页面
var nologinpages = ['/index.html', '/'];

Array.prototype.contains = function ( needle ) {
  for (i in this) {
    if (this[i] == needle) return true;
  }
  return false;
}

var thispage = window.location.pathname;

//登录token
var token = localStorage.getItem(tokenStr);
if((!token || token == 'null' || token == null || token == '') && nologinpages.contains(thispage)){
    parent.location.href = bserUrl + 'page/login/login.html';
}

//jquery全局配置
$.ajaxSetup({
	dataType: "json",
	cache: false,
    headers: {
        "token": token
    },
    xhrFields: {
	    withCredentials: true
    },
    complete: function(xhr) {
        //token过期，则跳转到登录页面
        if(!xhr.responseJSON || xhr.responseJSON.code == 401){
            parent.location.href = bserUrl + 'page/login/login.html';
        }
    }
});



