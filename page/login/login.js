layui.config({
	base : "js/"
}).use(['form','layer'],function(){
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		$ = layui.jquery;
	
	//登录按钮事件
	form.on("submit(login)",function(data){
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
		$.get(apiHostUrl + loginUrl,data.field,function(res){
			if(res == null || res.code != 0){
				layer.msg('登录失败，请确认您输入的帐号和密码');
			}
			if(res.code == 0 && res.success){
				localStorage.setItem(tokenStr, res.token);
				window.sessionStorage.setItem(userInfo,JSON.stringify(res.userInfo))
				window.location.href = bserUrl + "index.html";
			}
		});
		
		return false;
	})
})
